﻿using System;
using System.IO.Ports;
using System.Windows.Forms;
using OpenHardwareMonitor.Hardware;


namespace Arduino_PC_Monitor
{
    public partial class MainForm : Form
    {
        ToolStripStatusLabel toolStripStatusLabel;

        SerialPort serialPort = new SerialPort();
        Computer computer = new Computer()
        {
            GPUEnabled = true,
            CPUEnabled = true
        };

        /// <summary>
        /// CPU/GPU Temperature
        /// </summary>
        double val1;
        /// <summary>
        /// CPU/GPU Clock
        /// </summary>
        double val2;
        /// <summary>
        /// CPU/GPU Load
        /// </summary>
        double val3;
        /// <summary>
        /// If true send CPU data to Arduino
        /// </summary>
        bool sendCPU;
        /// <summary>
        /// True when this app is connected to Arduino
        /// </summary>
        bool connected;
        /// <summary>
        /// Number of time that CPU or GPU data were subsequently sent
        /// </summary>
        int count;

        public MainForm()
        {
            InitializeComponent();
            toolStripStatusLabel = new ToolStripStatusLabel();
            Init();
        }

        private void Init()
        {
            comboBoxIntervall.SelectedIndex = 0;
            sendCPU = true;
            connected = false;
            count = 0;
            try                                             // Try to set serial port
            {
                notifyIcon.Visible = false;
                serialPort.Parity = Parity.None;
                serialPort.StopBits = StopBits.One;
                serialPort.DataBits = 8;
                serialPort.Handshake = Handshake.None;
                serialPort.RtsEnable = true;
                string[] ports = SerialPort.GetPortNames();
                foreach (string p in ports)
                {
                    comboBoxPort.Items.Add(p);              // Populate port selector
                }
                serialPort.BaudRate = 9600;                 // Arduino is listening on port 9600
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void buttonCommect_Click(object sender, EventArgs e)
        {
            try                                             // Try to connect to Arduino
            {
                if (!serialPort.IsOpen)
                {
                    serialPort.PortName = comboBoxPort.Text;
                    serialPort.Open();
                    timer.Interval = int.Parse(comboBoxIntervall.Text) * 1000;
                    timer.Enabled = true;
                    toolStripStatusLabel.Text = "Sending data...";
                    label2.Text = "Connected";
                    connected = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonDisconnect_Click(object sender, EventArgs e)
        {
            try                                             // Disconnect from Arduino
            {
                if (connected)
                {
                    serialPort.Write("DIS'");
                    serialPort.Close();
                    connected = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            label2.Text = "Disconnected";
            timer.Enabled = false;
            toolStripStatusLabel.Text = "Connect to Arduino...";
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            try                                             // Disconnect from Arduino before closing app
            {
                if (connected)
                {
                    serialPort.Write("DIS'");
                    serialPort.Close();
                    connected = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            computer.Open();
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)  // When this windon is minimized
            {                                                   // Hide it and show an icon
                notifyIcon.Visible = true;                      // in windows notification area
                try
                {
                    notifyIcon.ShowBalloonTip(500, "Arduino", toolStripStatusLabel.Text, ToolTipIcon.Info);
                }
                catch (Exception) { }
                Hide();
            }
        }
        
        private void notifyIcon_DoubleClick(object sender, EventArgs e)
        {
            Show();                                 // When doubleclick the icon, hide it
            WindowState = FormWindowState.Normal;   // And show this form
            notifyIcon.Visible = false;
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            Status();
        }

        private void Status()
        {
            string s = "";
            foreach (var hardware in computer.Hardware)
            {
                hardware.Update();
                
                if (sendCPU)
                {
                    if (hardware.HardwareType != HardwareType.CPU)
                        continue;
                    hardware.Update();
                    if (count == 0)                             // Send name only if
                        s += hardware.Name.Split(' ')[2] + "'"; // it isn't allready sended

                    foreach (var sensor in hardware.Sensors)
                    {
                        if (sensor.SensorType == SensorType.Temperature && sensor.Name == "CPU Package")// Package temperature
                            val1 = Math.Round(sensor.Value.GetValueOrDefault());
                        if (sensor.SensorType == SensorType.Clock && sensor.Name.Contains("CPU Core"))  // Pick only CPU
                            val2 = Math.Round(sensor.Value.GetValueOrDefault());                        // Core's clock
                        if (sensor.SensorType == SensorType.Load && sensor.Name == "CPU Total")         // CPU Total load
                            val3 = Math.Round(sensor.Value.GetValueOrDefault());
                    }
                }
                else
                {
                    if (hardware.HardwareType != HardwareType.GpuAti && hardware.HardwareType != HardwareType.GpuNvidia)    // Not tested on Ati's GPU
                        continue;                                                                                           // Plaase report any issues
                    hardware.Update();
                    if (count == 0)                                                                 // Send name only if
                        s += hardware.Name.Split(' ')[2] + " " + hardware.Name.Split(' ')[3] + "^"; // it isn't allready sended
                                                                                                    // This work for my GTX 1060
                                                                                                    // If it doesn't work for your
                                                                                                    // GPU create an issues on GitLab
                    foreach (var sensor in hardware.Sensors)
                    {
                        if (sensor.SensorType == SensorType.Temperature && sensor.Name == "GPU Core")   // GPU Core Temperature
                            val1 = Math.Round(sensor.Value.GetValueOrDefault());
                        if (sensor.SensorType == SensorType.Clock && sensor.Name == "GPU Core")         // GPU Core Clock
                            val2 = Math.Round(sensor.Value.GetValueOrDefault());
                        if (sensor.SensorType == SensorType.Load && sensor.Name == "GPU Core")          // GPU Load
                            val3 = Math.Round(sensor.Value.GetValueOrDefault());
                    }
                }

                s += $"{val1}*{val2}#{val3}@";  // Format string for Arduino
                count++;                        // Increment counter
                if (count > 1)                  // If CPU or GPU data has been sent two time subsequently
                {
                    sendCPU = !sendCPU;         // Change the next output
                    count = 0;                  // Reset counter
                }
                break;
            }
            try
            {
                if (connected)
                    serialPort.Write(s);    //Send data to Arduino 
            }
            catch (Exception ex)
            {
                timer.Stop();
                MessageBox.Show(ex.Message);
                toolStripStatusLabel.Text = "Arduino's not responding...";
            }
        }
    }
}
