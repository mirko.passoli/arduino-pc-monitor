# Arduino PC Monitor
Monitor your PC hardware with Arduino and an LCD 16x2

Monitora i componenti del tuo PC con Arduino e un LCD 16x2
## English
### Components
1 Arduino

1 LCD 16x2 with I2C interface

4 Cables (pay attention to the pin on the display, it may require male-female cables)
### Setup Arduino
Nothing complicated for the physical connection, connect the Vcc from the LCD to the 5V pin in Arduino, the GND pin to the GND pin, and the SCL and SDA pins respectively to the A5 and A4 Arduino's pins
### Setup software
#### Arduino
1) Download the project and extract the contents of the Release folder

2) Download the LiquidCrystal_I2C library from the display manufacturer's website for your display

3) Open the [stretch](https://gitlab.com/mirko.passoli/arduino-pc-monitor/blob/master/Stretch_Arduino/Stretch_Arduino.ino) in the Arduino IDE and import the downloaded library

4) Connect Arduino to PC via USB and load the stretch

**Important:** the USB cable must always be connected so that the PC can communicate with Arduino
#### Windows PC
1) Launch the [Arduino PC Monitor.exe](https://gitlab.com/mirko.passoli/arduino-pc-monitor/tree/master/Release/Arduino%20PC%20Monitor.exe) file with administrator permissions

2) Select the port from the first dropdown on the top left corner (refer to the port shown in the Arduino IDE)

3) Select the update interval, in seconds, from the dropdown in the lower left corner

4) Connect to Arduino with the green tick button

To disconnect from Arduino use the red cross button or close the program

When the program is minimized, an icon next to the Windows's clock will appear, double-click it to open the program's window
## Italiano
### Componenti
1 Arduino

1 LCD 16x2 con interfaccia I2C

4 Cavi (attenzione ai pin sul display, potrebbero essere necessari dei cavi maschio-femmina)
### Impostare Arduino
Per i collegamenti fisici nulla di complicato, collegare i pin Vcc del display al 5V di Arduino, il GND al GND, e i pin SCL e SDA rispettivamente ai pin A5 e A4 di Arduino
### Impostare il software
#### Arduino
1) Scaricare il progetto e estrarre il contenuto della cartella Release

2) Scaricare dal sito del produttore del display la libreria LiquidCrystal_I2C per il propio display

3) Aprire lo [stretch](https://gitlab.com/mirko.passoli/arduino-pc-monitor/blob/master/Stretch_Arduino/Stretch_Arduino.ino) nell'IDE di Arduino ed importare la libreria scaricata

4) Collegare Arduino al PC tramite USB e caricare lo stretch

**N.B.** Il cavo dovrà essere sempre connesso in modo che il PC possa comunicare con Arduino
#### PC Windows
1) Avviare il file [Arduino PC Monitor.exe](https://gitlab.com/mirko.passoli/arduino-pc-monitor/tree/master/Release/Arduino%20PC%20Monitor.exe) con i permessi di amministratore

2) Selezionare la porta dalla prima dropdown in alto a sinistra (fare riferimento alla porta mostrata nell'IDE di Arduino)

3) Selezionare l'intervallo d'aggiornamento, in secondi, dalla dropdown in basso a sinistra

4) Collegarsi ad Arduino con il bottone contenente la spunta verde

Per disconnettersi da Arduino usare il bottone con la croce rossa o chiudere il programma

Quando il programma viene ridotto a icona comparirà un icona vicino all'orologio di Windows, fare doppio click per riaprire la finestra